import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext

import groovy.json.JsonSlurper


class AfterTestCaseListener {
	/**
	 * Executes after every test case ends.
	 * @param testCaseContext related information of the executed test case.
	 */
	@AfterTestCase
	def sampleAfterTestCase(TestCaseContext testCaseContext) {
		
		Date date = new Date()
		
		println date.toString()
		
		def formattedDate = date.format("yyyy-MM-dd") + 'T' + date.format("hh:mm:ss.S")
		println formattedDate
		
		//2018-06-06T09:23:09.510353300
		// "yyyy-mm-ddThh:mm:ss.S"
		
		String strTestCaseId = testCaseContext.getTestCaseId()
		String strTestCaseStatus = testCaseContext.getTestCaseStatus()
		
		
		def tcMap = testCaseContext.getTestCaseVariables()
		String strTestSuiteId = tcMap.get('variableTest')
		
		
		//println strTestCaseStatus
		
		//println strTestCaseId
		
		String body = '{"testClass": "katalon.practice","description": "' + strTestCaseId + '","status": "' + strTestCaseStatus + '","executionTime": "' + formattedDate + '","category": "practice","testRunId": "' + strTestSuiteId +'"}'
		
		println body
		
		def http = new URL(GlobalVariable.elasticsearch_url).openConnection() as HttpURLConnection
		http.setRequestMethod('POST')
		http.setDoOutput(true)
		http.setRequestProperty("Accept", 'application/json')
		http.setRequestProperty("Content-Type", 'application/json')
	
		http.outputStream.write(body.getBytes("UTF-8"))
		http.connect()
	
		
		def response = [:]
	
		println http.responseCode
		println http.responseMessage
		
		//if (http.responseCode == 200) {
		//	response = new JsonSlurper().parseText(http.inputStream.getText('UTF-8'))
		//} else {
		//	response = new JsonSlurper().parseText(http.errorStream.getText('UTF-8'))
		//}
		
	}
}